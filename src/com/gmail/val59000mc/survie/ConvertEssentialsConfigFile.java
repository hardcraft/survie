package com.gmail.val59000mc.survie;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.gmail.val59000mc.spigotutils.Logger;

public class ConvertEssentialsConfigFile {

	public static void start() {
		Bukkit.getScheduler().runTaskAsynchronously(Survie.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				convert();				
			}
		});
	}

	private static void convert() {
		File dir = new File("plugins/Essentials/userdata");
		if(dir.exists() && dir.isDirectory()){
			
			File[] files = dir.listFiles();
			int i= 1;
			int size = files.length;
			for(File file : files){
				Logger.info("Converting file "+i+"/"+size);
				convertYml(file);
				i++;
			}
		}
	}

	private static void convertYml(File file) {
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

		String lastLocation = cfg.getString("lastlocation.world");
		if(lastLocation != null){
			cfg.set("lastlocation.world", lastLocation.replace("world", "survie"));
		}
		
		String logoutLocation = cfg.getString("logoutlocation.world");
		if(logoutLocation != null){
			cfg.set("logoutlocation.world", logoutLocation.replace("world", "survie"));
		}
		
		ConfigurationSection homes = cfg.getConfigurationSection("homes");
		if(homes != null){
			for(String homeName : homes.getKeys(false)){
				String home = homes.getString(homeName+".world");
				if(home != null){
					homes.set(homeName+".world", home.replace("world","survie"));
				}
			}
		}
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
