package com.gmail.val59000mc.survie;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.survie.commands.ListEntitiesExecutor;
import com.gmail.val59000mc.survie.commands.LoveExecutor;
import com.gmail.val59000mc.survie.commands.PvPPurgeExecutor;
import com.gmail.val59000mc.survie.commands.TeamExecutor;
import com.gmail.val59000mc.survie.listeners.AACKickListener;
import com.gmail.val59000mc.survie.listeners.JoinListener;
import com.gmail.val59000mc.survie.listeners.PlayerChatListener;
import com.gmail.val59000mc.survie.listeners.TaskClearLag;
import com.gmail.val59000mc.survie.managers.EventManager;
import com.gmail.val59000mc.survie.managers.HCTeamManager;
import com.gmail.val59000mc.survie.managers.LoveBoorinooManager;
import com.gmail.val59000mc.survie.managers.LoveVal59000Manager;


public class Survie extends JavaPlugin{
	
	private static Survie plugin;
	public void onEnable(){
		plugin = this;
		createHardcraftFolder();
		
		new HCTeamManager();
		new LoveBoorinooManager();
		new LoveVal59000Manager();
		new EventManager();
		
		getCommand("listentities").setExecutor(new ListEntitiesExecutor());
		getCommand("team").setExecutor(new TeamExecutor());
		getCommand("love").setExecutor(new LoveExecutor());
		getCommand("pvppurge").setExecutor(new PvPPurgeExecutor());
		getCommand("event").setExecutor(new com.gmail.val59000mc.survie.commands.EventExecutor());
		
		//ConvertEssentialsConfigFile.start();
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(this, new TaskClearLag(), 18000);
		
		Bukkit.getPluginManager().registerEvents(new PlayerChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new AACKickListener(), this);
		//Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
		
	}
	
	public static Survie getPlugin(){
		return plugin;
	}
	
	public void onDisable(){
		Bukkit.getLogger().info("Plugin Survie disabled");
	}
	
	public void createHardcraftFolder(){
		File playersDir = new File("plugins/Hardcraft/VIPplayers");
		if(!playersDir.exists())
			playersDir.mkdirs();
	}	
	
	
}
