package com.gmail.val59000mc.survie;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultManager {


	private static VaultManager instance = null;
	private static Chat chat = null;
    
    public static VaultManager getInstance(){
    	if(instance == null){
    		instance = new VaultManager();
    		instance.setupChat();
    	}
    	return instance;
    }
    

    private boolean setupChat()
    {
        RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

	public String getPrefix(Player player){
		if(chat != null){
			return chat.getPlayerPrefix(player);
		}
		return "";
	}

	public String getSuffix(Player player){
		if(chat != null){
			return chat.getPlayerSuffix(player);
		}
		return "";
	}

}
