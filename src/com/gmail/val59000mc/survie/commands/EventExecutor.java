package com.gmail.val59000mc.survie.commands;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.survie.events.EventChest;
import com.gmail.val59000mc.survie.events.EventType;
import com.gmail.val59000mc.survie.events.IEventHC;
import com.gmail.val59000mc.survie.exceptions.EventAlreadyScheduledException;
import com.gmail.val59000mc.survie.exceptions.NoEventScheduledException;
import com.gmail.val59000mc.survie.managers.EventManager;


public class EventExecutor implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
			Random r = new Random();
			
			if(args.length > 0){
				switch(args[0]){
				
				
					case "chest":
						if(sender.hasPermission("hardcraftpvp.survie.event.admin")){
							try{
								Long delay = 0l; // delay in seconds
								int numberOfChest = 1;
								
								if(args.length >= 2){
									// args[1] = delay
									// Parsing random delay , syntax : 120:3600
									String randArgs[] = args[1].split(":");
									if(randArgs.length == 2){
										Integer min = Math.abs(Integer.parseInt(randArgs[0]));
										Integer max = Math.abs(Integer.parseInt(randArgs[1]));
										if(min > max){
											Integer temp = min;
											min = max;
											max = temp;
										}
										delay = min.longValue()+r.nextInt(max-min);
									}else{
										delay = Long.parseLong(args[1]);
									}
								}
								
								if(args.length == 3){
									// args[2] = number of chests
									numberOfChest = Math.abs(Integer.parseInt(args[2]));
									if(numberOfChest > EventChest.MAX_ITEMS_IN_CHEST){
										numberOfChest = EventChest.MAX_ITEMS_IN_CHEST;
									}
									if(numberOfChest == 0){
										numberOfChest = 1;
									}
									
								}
								
								IEventHC event = new EventChest(numberOfChest);
								EventManager.getInstance().scheduleEvent(event, delay);
								success(sender,"Un event "+EventType.CHEST.toString()+" a été programmé pour démarrer dans "+Time.getFormattedTime(delay));
								
							}catch(IllegalArgumentException e){
								error(sender,"Syntax error : ");
								error(sender,"/event chest <delay_seconds> [number_of_chests]");
								error(sender,"/event chest <delay_min_second:delay_max_second> [number_of_chests]");
								
							} catch (EventAlreadyScheduledException e) {
								error(sender,e.getMessage());
								
							}
						}else{
							error(sender,"Tu n'as pas la permission d'utiliser cette commande");
						}							
						break;
						
						
						
						
					case "stop":
						if(sender.hasPermission("hardcraftpvp.survie.event.admin")){
							try{
								EventManager.getInstance().stopEvent();
								for(Player p : Bukkit.getOnlinePlayers()){
									success(p, "L'event a été annulé");
								}
							}catch(NoEventScheduledException e){
								error(sender,e.getMessage());
							}
						}else{
							error(sender,"Tu n'as pas la permission d'utiliser cette commande");
						}	
						break;
						
						
						
						
					case "hide":
						if(sender instanceof Player){
							Player player = (Player) sender;
							try{
								Boolean visibility = EventManager.getInstance().toggleScoreboardVisibility(player);
								success(player,"Le scoreboard est maintenant "+ ((visibility==true)?"visible":"masqué") +" !");
							}catch(NoEventScheduledException e){
								error(player,e.getMessage());
							}
						}else{
							error(sender,"Seul un joueur in-game peut utiliser cette commande");
						}
						break;

						
					case "info":
						if(sender.hasPermission("hardcraft.event.admin")){
							try{
								success(sender,EventManager.getInstance().getCurrentEventInfo());
							}catch(NoEventScheduledException e){
								error(sender,e.getMessage());
							}
						}else{
							error(sender,"Tu n'as pas la permission d'utiliser cette commande");
						}	
						break;
						
					case "help":
					default:
						help(sender);
						break;
						
						
				}
			}else{
				sender.sendMessage("Erreur de syntaxe dans la commande");
				help(sender);
			}
		
		return true;
	}
	
	private void error(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.RED + message);
	}

	private void success(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.GREEN + message);
	}
	
	private void help(CommandSender sender){
		success(sender,"---------------------------");
		success(sender,"HELP : event");
		success(sender,"/event chest <delay> [number_of_chests] "+ChatColor.WHITE+": Cr�er un event chasse au coffre");
		success(sender,"/event stop "+ChatColor.WHITE+": Arr�ter l'event en cours");
		success(sender,"/event info "+ChatColor.WHITE+": Info sur l'event en cours");
		success(sender,"/event hide "+ChatColor.WHITE+": Afficher/Masquer le scoreboard");
		success(sender,"/event help "+ChatColor.WHITE+": Afficher cette page d'aide");
		success(sender,"---------------------------");
		
	}
	
	
	
	
}
