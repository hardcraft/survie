package com.gmail.val59000mc.survie.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;


public class ListEntitiesExecutor implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
			if(sender instanceof Player){
				Player player = (Player) sender;
				if(player.hasPermission("hardcraftpvp.survie.listentities")){
					
					List<EntityType> filters = new ArrayList<EntityType>();
					for(int i=0 ; i< args.length ; i++){
						try{
							filters.add(EntityType.valueOf(args[i].toUpperCase()));
						}catch(IllegalArgumentException e){
							player.sendMessage(ChatColor.RED+args[i]+" n'est pas une entité");
						}
					}
					listEntities(player,filters);		
				}
			}
		
		return true;
	}
	
	public void listEntities(Player player, List<EntityType> filters){
		Map<EntityType,Long> counts = new HashMap<EntityType,Long>();
		
		for(int i=0 ; i< Bukkit.getWorlds().size() ;i++){
			for(Entity entity : Bukkit.getWorlds().get(i).getEntities()){				
				EntityType type = entity.getType();
				if(filters.isEmpty() || (!filters.isEmpty() && filters.contains(type))){
					Long count = counts.get(type);
					if(count == null)
						count = 0l;
					counts.put(type, count + 1);
				}
			}
		}
		
		player.sendMessage(ChatColor.LIGHT_PURPLE+"Liste des entités");
		
		for(Entry<EntityType,Long> entry : counts.entrySet()){
			String color = "";
			Long value = entry.getValue();
			if(value <= 100){
				color += ChatColor.GREEN;
			}else if(value > 100 && value < 200){
				color += ChatColor.YELLOW;
			}else if(value > 200 && value < 300){
				color += ChatColor.GOLD;
			}else{
				color += ChatColor.RED;
			}
			player.sendMessage(entry.getKey().name()+" : "+color+entry.getValue());
		}
	}
	
	
}
