package com.gmail.val59000mc.survie.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.survie.exceptions.WaitLoveException;
import com.gmail.val59000mc.survie.managers.LoveBoorinooManager;
import com.gmail.val59000mc.survie.managers.LoveVal59000Manager;


public class LoveExecutor implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
			if(sender instanceof Player){
				Player player = (Player) sender;
				
				if(args.length == 1){
					String name = args[0];
					switch(name){
						case "Boorinoo":
							try {
								LoveBoorinooManager.getInstance().loveBoorinoo(player);
								for(Player p : Bukkit.getOnlinePlayers()){
									p.sendMessage(ChatColor.GREEN+player.getDisplayName()+" déclare son amour à "+ChatColor.RED+"Boorinoo"+ChatColor.GREEN+" !");
								}
							} catch (WaitLoveException e) {
								player.sendMessage(e.getMessage());
							}
							break;
						case "val59000":
							try {
								LoveVal59000Manager.getInstance().loveVal59000(player);
								for(Player p : Bukkit.getOnlinePlayers()){
									p.sendMessage(ChatColor.GREEN+player.getDisplayName()+" déclare son amour à "+ChatColor.RED+"val59000"+ChatColor.GREEN+" !");
								}
							} catch (WaitLoveException e) {
								player.sendMessage(e.getMessage());
							}
							break;
						default:
							syntaxError(player);
							break;
					}
				}else{
					syntaxError(player);
				}
				
			}
		
		return true;
	}
	
	public void syntaxError(Player player){
		player.sendMessage(ChatColor.RED+"Syntaxe: '/love Boorino' ou '/love val59000'");
	}
	
	
}
