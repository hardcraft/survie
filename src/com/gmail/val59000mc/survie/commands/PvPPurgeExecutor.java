package com.gmail.val59000mc.survie.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.NoChance.PvPManager.PvPManager;
import me.NoChance.PvPManager.Managers.PlayerHandler;

public class PvPPurgeExecutor implements CommandExecutor{

	PvPManager pvpm;
	
	public PvPPurgeExecutor() {
		
		Plugin plugin = Bukkit.getPluginManager().getPlugin("PvPManager");
        if(plugin != null || plugin instanceof PvPManager) {
        	pvpm = (PvPManager) plugin;
        }
        
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		if(pvpm == null){
			sender.sendMessage("§cPvPManager is not enabled !");
			return true;
		}
		
		if(args.length == 1){
			Player player = Bukkit.getPlayer(args[0]);
			if(player == null){
				sender.sendMessage("§c"+player+" is not online !");
			}else{
				PlayerHandler handler = pvpm.getPlayerHandler();
				handler.untag(handler.get(player));
				sender.sendMessage("§a"+player.getName()+" has been untagged !");
			}
		}else{
			sender.sendMessage("§cYou must provide a player : /pvppurge <player>");
		}
		
		return true;
	}	

}
