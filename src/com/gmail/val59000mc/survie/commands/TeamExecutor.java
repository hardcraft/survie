package com.gmail.val59000mc.survie.commands;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.survie.managers.HCTeamManager;
import com.gmail.val59000mc.survie.players.HCPlayer;
import com.gmail.val59000mc.survie.players.HCTeam;

public class TeamExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (args.length > 0) {
				switch (args[0]) {
					case "chat":
						if (args.length == 1) {
							if (sender.hasPermission("hardcraftpvp.survie.team.chat.write"))
								switchTeamChat(player);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team chat'");
						}
						break;
					case "create":
						if (args.length == 2) {
							// args[1] = team name
							if (sender.hasPermission("hardcraftpvp.survie.team.create"))
								createTeam(player,args[1]);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team create <name>'");
						}
						break;
					case "invite":
						if (args.length == 2) {
							// args[1] = player name
							if (sender.hasPermission("hardcraftpvp.survie.team.invite"))
								inviteTeam(player,args[1]);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team invite <player>'");
						}
						break;
					case "accept":
						if (args.length == 2) {
							// args[1] = player name
							if (sender.hasPermission("hardcraftpvp.survie.team.accept"))
								acceptTeam(player,args[1]);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team accept <player>'");
						}
						break;
					case "leave":
						if (args.length == 1) {
							if (sender.hasPermission("hardcraftpvp.survie.team.leave"))
								leaveTeam(player);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team leave'");
						}
						break;
					case "tp":
						if (args.length == 1) {
							if (sender.hasPermission("hardcraftpvp.survie.team.tp"))
								tpTeam(player);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team tp'");
						}
						break;
					case "home":
						if (args.length == 1) {
							if (sender.hasPermission("hardcraftpvp.survie.team.home.tp"))
								homeTeam(player);
							else if (sender.hasPermission("hardcraftpvp.survie.team.home.see")){
								seehomeTeam(player);
							}else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team home'");
						}
						break;
					case "sethome":
						if (args.length == 1) {
							if (sender.hasPermission("hardcraftpvp.survie.team.sethome"))
								sethomeTeam(player);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team sethome'");
						}
						break;
					case "list":
						if (args.length == 1) {
							if (sender.hasPermission("hardcraftpvp.survie.team.list"))
								listTeam(player);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						}else if(args.length == 2){
							// args[1] = team name
							if (sender.hasPermission("hardcraftpvp.survie.team.list"))
								listTeam(player,args[1]);
						}
						break;
					case "rename":
						if (args.length == 2) {
							// args[1] = name
							if (sender.hasPermission("hardcraftpvp.survie.team.rename"))
								renameTeam(player, args[1]);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team rename <name>'");
						}
						break;
					case "kick":
						if (args.length == 2) {
							// args[1] = playerName
							if (sender.hasPermission("hardcraftpvp.survie.team.kick"))
								kickPlayer(player, args[1]);
							else
								error(sender, "Vous n'avez pas la permission pour utiliser cette commande.");
						} else {
							error(sender, "Cette commande n'existe pas. Tapez '/team kick <player>'");
						}
						break;
					case "help":
						helpTeam(player);
						break;
					default:
						error(sender, "Cette commande n'existe pas");
						helpTeam(player);
						break;
				}
			}else{
				error(sender, "Cette commande n'existe pas");
				helpTeam(player);
			}
		}

		return true;
	}

	public void helpTeam(Player player) {
		success(player,"--------------------------");
		success(player,"/team create <nom> : "+ChatColor.WHITE+"créer une team");
		success(player,"/team leave : "+ChatColor.WHITE+"quitter la team");
		success(player,"/team invite <joueur> : "+ChatColor.WHITE+"invite un joueur dans la team");
		success(player,"/team accept <joueur> : "+ChatColor.WHITE+"accepter l'invitation dans une team");
		success(player,"/team kick <joueur> : "+ChatColor.WHITE+"exclure un joueur de la team");
		success(player,"/team rename <nom> : "+ChatColor.WHITE+"renommer la team");
		success(player,"/team sethome : "+ChatColor.WHITE+"créer le home de la team");
		success(player,"/team home : "+ChatColor.WHITE+"se tp au home de la team");
		success(player,"/team tp : "+ChatColor.WHITE+"envoyer une demande de tpahere à tous les membres");
		success(player,"/team list [team] : "+ChatColor.WHITE+"infos sur une team");
		success(player,"/team chat : "+ChatColor.WHITE+"parler dans le chat de la team");
		success(player,"--------------------------");
	}

	public static void error(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.RED + message);
	}

	public static void success(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.GREEN + message);
	}
	
	public void switchTeamChat(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCPlayer hcPlayer = manager.switchTeamChat(player);
			
			if(hcPlayer.isTalkingInTeamChat()){
				success(player, "Tu parles maintenant dans le chat de team.");
			}else{
				success(player, "Tu parles maintenant dans le chat global.");
			}
		}else{
			error(player, "Tu n'est dans aucune team");
		}
	}
	
	public void createTeam(Player player, String name){
		if(name.length() != 3){
			error(player, "Le nom de la team doit être de 3 caractères exactement.");
		}else if(!StringUtils.isAlphanumeric(name)){
			error(player, "Le nom de la team ne peut contenir que des chiffres et des lettres.");
		}else{
			HCTeamManager manager = HCTeamManager.getInstance();
			if(manager.hasTeam(player)){
				error(player, "Tu es déjà dans une team");
			}else if(manager.teamNameExists(name)){
				error(player, "Ce nom de team existe déjà");
			}else{
				try {
					manager.createTeam(player,name);
					success(player, "La team a été créée avec succès");
				} catch (IOException e) {
					error(player, "Une erreur est survenue lors de la création de la team. Contactez un admin.");
					e.printStackTrace();
				}
			}
		}
	}
	
	public void renameTeam(Player player, String name){
		if(name.length() != 3){
			error(player, "Le nom de la team doit être de 3 lettres exactement.");
		}else if(!StringUtils.isAlphanumeric(name)){
			error(player, "Le nom de la team ne peut contenir que des chiffres et des lettres.");
		}else{
			HCTeamManager manager = HCTeamManager.getInstance();
			if(manager.hasTeam(player)){
				HCTeam team = manager.getTeam(player);
				if(team.isLeader(player)){
					if(!manager.teamNameExists(name)){
						try {
							team.rename(name);
							success(player, "La team a été renommée");
						} catch (IOException e) {
							error(player, "Une erreur est survenue lors de l'édition du nom de la team. Contactez un admin.");
							e.printStackTrace();
						}
					}else{
						error(player, "Ce nom de team existe déjà.");
					}
				}else{
					error(player, "Tu n'es pas le leader de la team.");
				}
			}else{
				error(player, "Tu n'es pas dans une team");
			}
		}
	}
	
	public void kickPlayer(Player player, String playerName){
		Player kicked = Bukkit.getPlayer(playerName);
		if(kicked == null){
			error(player,playerName+" n'est pas en ligne");
		}else{
			HCTeamManager manager = HCTeamManager.getInstance();
			if(manager.hasTeam(player)){
				HCTeam team = manager.getTeam(player);
				if(team.isLeader(player)){
					if(kicked.getUniqueId().equals(player.getUniqueId())){
						error(player,"Tu ne peut pas te kick toi-même");
					}else{
						if(team.isInTeam(kicked)){
							try {
								team.leave(kicked);
								kicked.sendMessage(ChatColor.RED+"Vous avez été exclu de la team "+team.getName());
								team.broadcastMessage(ChatColor.GREEN+kicked.getName()+" a été exclu de la team",null);
							} catch (IOException e) {
								error(player, "Une erreur de fichier est survenue lors du kick du joueur. Contactez un admin.");
								e.printStackTrace();
							}
						}else{
							error(player,playerName+" n'est pas dans ta team.");
						}
					}
				}else{
					error(player,"Seul le leader de la team peut kick des membres.");
				}
			}else{
				error(player,"Tu n'est dans aucune team.");
			}
			
		}
	}
	
	
	public void inviteTeam(Player player,String playerName){
		Player invited = Bukkit.getPlayer(playerName);
		if(invited == null){
			error(player, playerName+" n'est pas en ligne.");
		}else{
			if(player.getUniqueId().equals(invited.getUniqueId())){
				error(player, "Tu ne peux pas t'inviter toi-même.");
			}else{
				HCTeamManager manager = HCTeamManager.getInstance();
				if(manager.hasTeam(player)){
					
					if(manager.hasTeam(invited)){
						error(player, playerName+" est déjà dans une team.");
					}else{
						HCTeam team = manager.getTeam(player);
						if(team.isLeader(player)){
							if(team.isInvited(invited)){
								error(player,"Ce joueur vient déjà de recevoir une invitation.");
							}else{
								team.invite(invited);
								success(player,"Une invitation a été envoyée à "+playerName);
							}
						}else{
							error(player,"Seul le leader peut inviter des joueurs dans la team.");
						}
					}
				}else{
					error(player, "Vous n'êtes pas dans une team.");
				}
			}
		}
	}
	

	
	public void leaveTeam(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCTeam team = manager.getTeam(player);
			if(team.isLeader(player)){
				try {
					manager.removeTeam(team);
					team.broadcastMessage(ChatColor.GREEN+player.getName()+" a supprimé la team",player);
					success(player,"Vous avez supprimé la team");
					
				} catch (IOException e) {
					error(player, "Une erreur est survenue lors de la suppression de la team. Contactez un admin.");
					e.printStackTrace();
				}
			}else{
				try {
					team.leave(player);
					team.broadcastMessage(ChatColor.GREEN+player.getName()+" a quitté la team",player);
					success(player, "Vous avez quitté la team");
					
				} catch (IOException e) {
					error(player, "Une erreur de fichier est survenue au moment de quitter la team. Contactez un admin.");
					e.printStackTrace();
				}
			}
		}else{
			error(player,"Tu n'es pas dans une team.");
			
		}
	}
	
	public void acceptTeam(Player player,String playerName){
		Player inviter = Bukkit.getPlayer(playerName);
		if(inviter == null){
			error(player, playerName+" n'est pas en ligne.");
		}else{
			HCTeamManager manager = HCTeamManager.getInstance();
			if(manager.hasTeam(player)){
				error(player,"Tu es déjà dans une team");
			}else if(!manager.hasTeam(inviter)){
				error(player, playerName+" n'est pas dans une team.");
			}else{
				HCTeam team = manager.getTeam(inviter);
				if(team.isLeader(inviter)){
					if(team.isInvited(player)){
						try {
							team.join(player);
							team.broadcastMessage(ChatColor.GREEN+player.getName()+" a rejoint la team",null);
						} catch (IOException e) {
							error(player, "Une erreur de fichier est survenue lors de l'invitation dans la team. Contactez un admin.");
							e.printStackTrace();
						}
					}else{
						error(player,"Tu n'as pas été invité dans la team de "+playerName);
					}
				}else{
					error(player, playerName+" n'est pas leader de sa team. Il ne peut pas t'inviter.");
				}
			}
		}
	}
	
	public void tpTeam(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCTeam team = manager.getTeam(player);
			if(team.getMembers().size() == 0){
				error(player, "Il n'y a personne d'autre dans ta team");
			}else{
				team.summonPlayers(player);
				success(player, "Une requête de téléportation a été envoyée aux membres présents de ta team.");
			}
		}else{
			error(player, "Tu n'es dans aucune team");
		}
	}
	
	public void homeTeam(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCTeam team = manager.getTeam(player);
			if(team.getHome() == null){
				error(player, "Ta team n'a pas de home");
			}else{
				player.teleport(team.getHome());
				success(player, "Téléportation vers le home de la team.");
			}
		}else{
			error(player, "Tu n'es dans aucune team");
		}
	}
	
	public void seehomeTeam(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCTeam team = manager.getTeam(player);
			if(team.getHome() == null){
				error(player, "Ta team n'a pas de home");
			}else{
				success(player, "Le home de ta team est en : "+
						team.getHome().getWorld().getName()+
						" "+team.getHome().getBlockX()+
						" "+team.getHome().getBlockY()+
						" "+team.getHome().getBlockZ());
				success(player, "Il faut être VIP+ pour pouvoir s'y téléporter");
			}
		}else{
			error(player, "Tu n'es dans aucune team");
		}
	}
	
	public void sethomeTeam(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCTeam team = manager.getTeam(player);
			if(team.isLeader(player)){
				try {
					team.modifyHome(player.getLocation());
					team.broadcastMessage(ChatColor.GREEN+player.getName()+" a modifié le home de la team",null);
				} catch (IOException e) {
					error(player, "Une erreur de fichier est survenue au moment de modifier le home de la team. Contactez un admin.");
					e.printStackTrace();
				}
			}else{
				error(player, "Seul le leader peut modifier le home de la team");
			}
		}else{
			error(player, "Tu n'es dans aucune team");
		}
	}
	
	public void listTeam(Player player){
		HCTeamManager manager = HCTeamManager.getInstance();
		if(manager.hasTeam(player)){
			HCTeam team = manager.getTeam(player);
			success(player,"---------------------------");
			success(player,"Team : "+team.getName());
			success(player,"Leader : "+team.getLeader().getName());
			success(player,"Membres :");
			synchronized(team.getMembers()){
				for(HCPlayer hcPlayer : team.getMembers()){
					success(player, "- "+hcPlayer.getName());
				}
			}
			success(player,"---------------------------");
		}else{
			error(player, "Tu n'es dans aucune team");
		}
	}
	
	public void listTeam(Player player, String name){
		HCTeamManager manager = HCTeamManager.getInstance();
		HCTeam team = manager.getTeam(name);
		
		if(team != null){
			success(player,"---------------------------");
			success(player,"Team : "+team.getName());
			success(player,"Leader : "+team.getLeader().getName());
			success(player,"Membres :");
			synchronized(team.getMembers()){
				for(HCPlayer hcPlayer : team.getMembers()){
					success(player, "- "+hcPlayer.getName());
				}
			}
			success(player,"---------------------------");
		}else{
			error(player, "Cette team n'existe pas");
		}
	}

}
