package com.gmail.val59000mc.survie.dependencies;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PermissionsExManager {
    
	public static String getPrefix(String playerName){
		 PermissionUser user = PermissionsEx.getUser(playerName);
		 return ChatColor.translateAlternateColorCodes('&', user.getPrefix());
	}

	public static String getShortPrefix(String playerName) {
		String prefix = PermissionsEx.getUser(playerName).getPrefix();
		prefix = prefix.replace("Modérateur", "Modo").replace("Administrateur", "Admin");
		return prefix;
	}
	
	
	public static void addRankTime(String playerName, String group, Long timeInSeconds){
		// Equivalent to /pex user <user> group add <group> [world] [lifetime]
		// For all worlds use /pex user <user> group add <group> "" [lifetime]
		Long remaining = getRemainingTimeRank(playerName,group);
		remaining = (remaining == null) ? 0L : remaining;
		PermissionsEx.getUser(playerName).addGroup(group, null, remaining+timeInSeconds);
		PermissionsEx.getUser(playerName).save();
	}
	
	public static void removeRank(String playerName, String group){
		PermissionsEx.getUser(playerName).removeGroup(group);
		PermissionsEx.getUser(playerName).setOption("group-"+group+"-until", null);
	}
	
	public static Long getRemainingTimeRank(String playerName, String group){
		// Option : group-VIP-until
		// Option : group-VIP+-until
		Long now = Calendar.getInstance().getTimeInMillis()/1000;
		String timeStr = PermissionsEx.getUser(playerName).getOption("group-"+group+"-until", null);
		if(timeStr == null){
			return null;
		}else{
			Long until;
			try{
				until = Long.parseLong(timeStr);
			}catch(IllegalArgumentException e){
				until = 0L;
			}
			if(until < now){
				return 0L;
			}else{
				return until-now;
			}
		}
	}
	
	public static List<String> getRanks(String playerName){
		List<String> groups = new ArrayList<String>();
		for(PermissionGroup permGroup : PermissionsEx.getUser(playerName).getParents()){
			groups.add(permGroup.getName());
		}
		return groups;
	}
}
