package com.gmail.val59000mc.survie.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.survie.Survie;
import com.gmail.val59000mc.survie.listeners.EventChestListener;

public class EventChest extends EventHCImpl implements IEventHC  {

	private int numberOfChest;
	private Map<Location,Boolean> locations;
	public final static int MAX_ITEMS_IN_CHEST = 5;
	private Listener chestListener;
	
	public EventChest(int numberOfChests){
		super(EventType.CHEST);
		this.numberOfChest = numberOfChests;
		this.locations = new HashMap<Location,Boolean>();
		setSecondsBeforeStart(210);
		setSecondsBeforeEnd(900);
	}

	@Override
	public void start() {
		
		setState(EventState.RUNNING);
		
		// Creating random locations and spawning random chests
		for(int i = 0 ; i < numberOfChest ; i++){
			locations.put(getRandomLocation(),false);
		}
		
		// Spawn chests
		spawnRandomChests();
		
		// Starting threads
		Bukkit.getScheduler().runTask(Survie.getPlugin(), new EventRunningThread(this));
		
		// Start chest listener
		chestListener = new EventChestListener(this,locations.keySet());
		Bukkit.getPluginManager().registerEvents(chestListener, Survie.getPlugin());
	}
	
	private Location getRandomLocation(){
		World mainworld = Bukkit.getServer().getWorlds().get(0);
		Random r = new Random();
		
		int x = 0 , z = 0;
		while( x>=-200 && x <= 200){
			x = r.nextInt(1000) - 500;
		}
		while( z>=-200 && z <= 200){
			z = r.nextInt(1000) - 500;
		}
		
		return mainworld.getHighestBlockAt(x, z).getLocation().clone();
	}
	
	private void spawnRandomChests(){
		Map<ItemStack,List<Integer>> chestConfig = EventConfigurationReader.getEventChestConfig();
		List<ItemStack> keys = new ArrayList<ItemStack>(chestConfig.keySet());
		
		Random r = new Random();
		
		for(Location loc : locations.keySet()){
			World world = loc.getWorld();
			
			world.getBlockAt(loc).setType(Material.CHEST);
			Chest chest = (Chest) world.getBlockAt(loc).getState();
			Inventory inv = chest.getBlockInventory();
			ItemStack[] contents = inv.getContents();
			
			int itemsInChest = 0;
			while(chestConfig.size() > 0 && itemsInChest < EventChest.MAX_ITEMS_IN_CHEST){
				int randomIndex = r.nextInt(chestConfig.size());
				
				// Get random item
				ItemStack item = keys.get(randomIndex);
				List<Integer> range = chestConfig.get(item);
				item = item.clone();
				
				// Set amount
				if(range.size() == 2){
					int amount = range.get(0)+r.nextInt(range.get(1)-range.get(0));
					item.setAmount(amount);
				}else{
					item.setAmount(range.get(0));
				}
				
				// Set item in random position in chest
				randomIndex = r.nextInt(contents.length);
				int tries = 0;
				while(tries < 10 && contents[randomIndex] != null){
					randomIndex = r.nextInt(contents.length);
					tries++;
				}
				if(item.getAmount() != 0){
					contents[randomIndex] = item;
				}
				itemsInChest++;
			}
			
			inv.setContents(contents);
		}
	}
	
	private void removeChests(){
		for(Location loc : locations.keySet()){
			if(loc.getWorld().getBlockAt(loc).getType().equals(Material.CHEST)){
				Chest chest = (Chest) loc.getWorld().getBlockAt(loc).getState();
				chest.getInventory().setContents(new ItemStack[27]);
			}
			loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		}
				
	}

	
	protected List<String> getScoreboardContent(Player player){
		
		List<String> content = new ArrayList<String>();
		int index = 1;
		for(Entry<Location,Boolean> entry : locations.entrySet()){
			StringBuilder locDisplay = new StringBuilder();
			Location loc = entry.getKey();
			if(entry.getValue()){
				// if entry.getValue() == true, the chest has been found
				locDisplay.append(ChatColor.BOLD+""+ChatColor.GRAY+"Coffre "+index+" : ");
				locDisplay.append(ChatColor.RESET);
				locDisplay.append(ChatColor.STRIKETHROUGH);
			}else{
				locDisplay.append(ChatColor.BOLD+""+ChatColor.GOLD+"Coffre "+index+" : ");
				locDisplay.append(ChatColor.RESET);
			}
			locDisplay.append(loc.getBlockX()+" ");
			locDisplay.append(loc.getBlockZ()+" ");
			content.add(locDisplay.toString());
			content.add(" ");
			index++;
		}
		
		return content;
	}
	
	public void end(){
		PlayerInteractEvent.getHandlerList().unregister(chestListener);
		removeChests();
		super.end();
	}
	
	private void endNoRemoveChest(){
		PlayerInteractEvent.getHandlerList().unregister(chestListener);
		super.end();
	}
	
	public void remove(){
		removeChests();
		getScoreboard().destroy();
		super.remove();
	}

	public void chestFound(Player player, Location chestLocation) {
		if(locations.get(chestLocation) == false){
			locations.put(chestLocation,true);
			broadcastMessage(player.getDisplayName()+ChatColor.GREEN+" a trouv� un coffre de l'event !", Sound.NOTE_BASS_DRUM);
			checkAllChestsFound();
		}
	}
	
	public void checkAllChestsFound(){
		Boolean allFound = true;
		for(Boolean found : locations.values()){
			if(found == false)
				allFound = false;
		}
		if(allFound)
			endNoRemoveChest();
	}

	@Override
	public String getEventSummary() {
		StringBuilder str = new StringBuilder();
		str.append(ChatColor.GREEN+"Type: "+ChatColor.GRAY+this.getType().toString());
		str.append(ChatColor.WHITE+" - ");
		str.append(ChatColor.GREEN+"Etape: "+ChatColor.GRAY+this.getState().toString());
		str.append(ChatColor.WHITE+" - ");
		switch(this.getState()){
			case CREATED:
				str.append(ChatColor.GREEN+"Detail: "+ChatColor.GRAY+"L'event va d�marrer bient�t.");
				break;
			case TEASING:
				str.append(ChatColor.GREEN+"Detail: "+ChatColor.GRAY+"L'event commence dans "+Time.getFormattedTime(this.getSecondsBeforeStart()));
				break;
			case RUNNING:
				str.append(ChatColor.GREEN+"Detail: "+ChatColor.GRAY+"L'event se termine dans "+Time.getFormattedTime(this.getSecondsBeforeEnd()));
				break;
			case ENDED:
			case REMOVED:
			default:
				str.append(ChatColor.GREEN+"Detail: aucun");
				break;
		}
		
		return str.toString();
	}

}
