package com.gmail.val59000mc.survie.events;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.val59000mc.survie.Survie;

public class EventConfigurationReader {
	
	public static Map<ItemStack,List<Integer>> getEventChestConfig(){
		Map<ItemStack,List<Integer>> chestConfig = new HashMap<ItemStack,List<Integer>>();
		
		FileConfiguration cfg = getEventConfig();
		
		List<String> chestContentList = cfg.getStringList("events.chest");
		chestContentList = chestContentList == null ? new ArrayList<String>() : chestContentList;
		
		for(String line : chestContentList){
			// Line format = item_name/item_meta/amount_range/name/lore/enchantments 
			// range = min_value:max_value OR value
			// enchantements = enchantment_name:level,enchantment_name:level ...
			// lore = line,line,line
			String[] itemData = line.split(";");
			try{
				if(itemData.length == 6){
					
					// Parse item (itemData[0]) and item meta (itemData[1])
					ItemStack item = new ItemStack(Material.valueOf(itemData[0].toUpperCase()),1,Short.parseShort(itemData[1]));
					
					// Parse range (itemData[2])
					List<Integer> range = new ArrayList<Integer>();
					String[] rangeData = itemData[2].split(":");
					if(rangeData.length == 2){
						Integer min = Math.abs(Integer.parseInt(rangeData[0]));
						Integer max =  Math.abs(Integer.parseInt(rangeData[1]));
						if(min > max){
							Integer temp = min;
							min = max;
							max = temp;
						}
						range.add(min);
						range.add(max);
						
					}else{
						range.add(Math.abs(Integer.parseInt(itemData[2])));
					}
					
					// Parse name (itemData[3]) and lore (itemData[4])
					ItemMeta im = item.getItemMeta();
					if(!itemData[3].equalsIgnoreCase("NULL")){
						im.setDisplayName(itemData[3]);
					}
					if(!itemData[4].equalsIgnoreCase("NULL")){
						List<String> lore = new ArrayList<String>();
						String[] loreData = itemData[4].split(",");
						for(String loreLine : loreData){
							lore.add(loreLine);
						}
						im.setLore(lore);
					}
					
					//Parse enchantments (itemData[5])
					if(!itemData[5].equalsIgnoreCase("NULL")){
						String[] enchantsData = itemData[5].split(",");
						for(String enchantEntry : enchantsData){
							String[] enchantData = enchantEntry.split(":");
							if(enchantData.length == 2){
								if(item.getType().equals(Material.ENCHANTED_BOOK)){
									((EnchantmentStorageMeta) im).addStoredEnchant(Enchantment.getByName(enchantData[0]), Integer.parseInt(enchantData[1]) , true);
								}else{
									im.addEnchant(Enchantment.getByName(enchantData[0]), Integer.parseInt(enchantData[1]) , true);
								}
							}else{
								throw new IllegalArgumentException("Erreur : enchantement = enchantement:valeur");
							}
						}
					}
					
					item.setItemMeta(im);
					chestConfig.put(item, range);
					
				}else{
					throw new IllegalArgumentException("6 éments nécessaires dans la chaine formatée");
				}
			}catch(IllegalArgumentException e){
				Bukkit.getLogger().warning(e.getMessage());
				e.printStackTrace();
				Bukkit.getLogger().warning("Event chest - Parse impossible de "+line);
			}
		}
		return chestConfig;
	}
	
	private static FileConfiguration getEventConfig(){
		File file = new File(Survie.getPlugin().getDataFolder(),"events.yml");
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	
		return YamlConfiguration.loadConfiguration(file);
	}
}
