package com.gmail.val59000mc.survie.events;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.survie.Survie;

public abstract class EventHCImpl implements IEventHC{
	
	private EventType type;
	private EventState state;
	private EventScoreboard scoreboard;
	private long secondsBeforeEnd;
	private long secondsBeforeStart;
	
	protected EventHCImpl(EventType type){
		this.type = type;
		this.state = EventState.CREATED;
		this.secondsBeforeEnd = 1200;
		this.secondsBeforeStart = 600;
		this.scoreboard = new EventScoreboard(this);
	}
	
	public void startTeaser(){
		setState(EventState.TEASING);
		broadcastMessage(ChatColor.GRAY+"Tape "+ChatColor.ITALIC+"/event hide "+ChatColor.RESET+""+ChatColor.GRAY+" pour masquer le scoreboard.");
		Bukkit.getScheduler().runTask(Survie.getPlugin(),new EventTeaserThread(this));
	}
	
	public void startTeaserAfterDelay(long delayed) {
		Bukkit.getScheduler().runTaskLater(Survie.getPlugin(), new Runnable(){

			@Override
			public void run() {
				startTeaser();
			}
			
			
		}, delayed*20);
	}
	
	public abstract void start();
	
	public void remove(){
		setState(EventState.REMOVED);
	}
	
	public void end(){
		broadcastMessage( ChatColor.GREEN+"L'event "+
				ChatColor.WHITE+ChatColor.BOLD+getType()+
				ChatColor.RESET+ChatColor.GREEN+" est terminé ! ", Sound.ENDERDRAGON_GROWL);
		setState(EventState.ENDED);
		setSecondsBeforeEnd(0);
	}
	
	
	protected void broadcastMessage(String message){
		Bukkit.getLogger().info(ChatColor.stripColor(message));
		for(Player p : Bukkit.getOnlinePlayers()){
			p.sendMessage(message);
		}
	}
	
	protected void broadcastMessage(String message, Sound sound){
		Bukkit.getLogger().info(ChatColor.stripColor(message));
		for(Player p : Bukkit.getOnlinePlayers()){
			p.sendMessage(message);
			p.playSound(p.getLocation(), sound, 1f, 1.2f);
		}
	}
	
	public EventType getType() {
		return type;
	}

	public EventState getState() {
		return state;
	}

	protected void setState(EventState state) {
		this.state = state;
	}
	
	protected long getSecondBeforeEnd(){
		return secondsBeforeEnd;
	}
	
	protected abstract List<String> getScoreboardContent(Player player);
	public abstract String getEventSummary();

	public long getSecondsBeforeEnd() {
		return secondsBeforeEnd;
	}

	public void setSecondsBeforeEnd(long secondsBeforeEnd) {
		this.secondsBeforeEnd = secondsBeforeEnd;
	}
	
	public long getSecondsBeforeStart() {
		return secondsBeforeStart;
	}

	public void setSecondsBeforeStart(long secondsBeforeStart) {
		this.secondsBeforeStart = secondsBeforeStart;
	}
	
	public EventScoreboard getScoreboard(){
		return scoreboard;
	}
}
