package com.gmail.val59000mc.survie.events;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;

import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.survie.Survie;


public class EventRunningThread implements Runnable{

	private EventHCImpl event;
	private List<Long> displayAt = Arrays.asList(new Long[]{1l,2l,3l,4l,5l,10l,30l,60l,120l,300l});
	
	public EventRunningThread(EventHCImpl event){
		this.event = event;
	}
	
	@Override
	public void run() {		
			
		long timeLeft = event.getSecondsBeforeEnd();
		
		if(event.getState().equals(EventState.RUNNING)){
			
			if(timeLeft == 0l){
				event.end();
			}
			
			if(displayAt.contains(timeLeft)){
				event.broadcastMessage( ChatColor.GREEN+"L'event "+
						ChatColor.WHITE+ChatColor.BOLD+event.getType()+
						ChatColor.RESET+ChatColor.GREEN+" se termine dans "+
						ChatColor.WHITE+ChatColor.BOLD+Time.getFormattedTime(timeLeft)+
						ChatColor.RESET+ChatColor.GREEN+" !", Sound.CLICK);
			}
		}
		
		if(event.getState().equals(EventState.ENDED)){
			if(timeLeft <= -15l){
				event.remove();
			}
		}
		
		if(!event.getState().equals(EventState.REMOVED)){
			event.getScoreboard().updateAllPlayers();
			event.setSecondsBeforeEnd(event.getSecondBeforeEnd()-1);
			Bukkit.getScheduler().runTaskLater(Survie.getPlugin(), this,20);
		}
	}
	

}
