package com.gmail.val59000mc.survie.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Time;

public class EventScoreboard{
	List<String> content;
	EventHCImpl event;
	
	// One scoreboard per username
	Map<String,SimpleScoreboard> sc;
	
	public EventScoreboard(EventHCImpl event){
		this.event = event;
		this.content = new ArrayList<String>();
		this.sc = new HashMap<String,SimpleScoreboard>();
	}
	
	public void updateAllPlayers(){
		for(Player player : Bukkit.getOnlinePlayers()){
			update(player);
		}
	}
	
	public void update(Player player){
			SimpleScoreboard scoreboard = sc.get(player.getName());
			if(scoreboard == null){
				scoreboard = new SimpleScoreboard(ChatColor.GREEN+"Event");
				sc.put(player.getName(), scoreboard);
			}
			
			if(scoreboard.getVisible()){
				
				scoreboard.clear();
				this.content = event.getScoreboardContent(player);
				
				scoreboard.blankLine();
				scoreboard.add(ChatColor.GOLD+""+ChatColor.BOLD+"Event : "+ChatColor.RESET+event.getType().toString());
				scoreboard.blankLine();
				
				if(event.getState().equals(EventState.RUNNING)){
					scoreboard.add(ChatColor.GOLD+""+ChatColor.BOLD+"Temps : "+ChatColor.RESET+Time.getFormattedTime(event.getSecondsBeforeEnd()));
					scoreboard.blankLine();
					
			        for(String line : content){
			        	scoreboard.add(line);
			        }
			        
				}
				
				if(event.getState().equals(EventState.TEASING)){
					scoreboard.add(ChatColor.GOLD+""+ChatColor.BOLD+"Temps : "+ChatColor.RESET+Time.getFormattedTime(event.getSecondsBeforeStart()));
					scoreboard.blankLine();
				}
		        
				scoreboard.blankLine();
				scoreboard.add(ChatColor.GREEN+event.getState().toString());
		        
				scoreboard.draw();
				
				scoreboard.send(player);
				
			}
	}
	
	public Boolean toggleVisibility(Player player){
		if(sc.containsKey(player.getName())){
			SimpleScoreboard scoreboard = sc.get(player.getName());
			scoreboard.setVisible(!scoreboard.getVisible());
			if(scoreboard.getVisible() == false){
				player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
			}
			return scoreboard.getVisible();
		}
		return false;
	}
	
	public void destroy(){
		for(Entry<String,SimpleScoreboard> entry : sc.entrySet()){
			entry.getValue().clear();
			entry.getValue().destroy();
		}
		sc.clear();
	}
}	
