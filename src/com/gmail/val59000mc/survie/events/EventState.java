package com.gmail.val59000mc.survie.events;

public enum EventState {
	CREATED{
		@Override
		public String toString(){
			return "Nouveau";
		}
	},
	TEASING{
		@Override
		public String toString(){
			return "Commence bientot";
		}
	},
	RUNNING{
		@Override
		public String toString(){
			return "En cours";
		}
	},
	ENDED{
		@Override
		public String toString(){
			return "Fini";
		}
	},
	REMOVED{
		@Override
		public String toString(){
			return "Inactif";
		}
	},
	
}
