package com.gmail.val59000mc.survie.events;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;

import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.survie.Survie;

public class EventTeaserThread implements Runnable{

	private List<Long> displayAt = Arrays.asList(new Long[]{1l,2l,3l,4l,5l,10l,30l,60l,120l,300l,600l,900l,1200l});
	private EventHCImpl event;
	
	public EventTeaserThread(EventHCImpl event){
		this.event = event;
	}
	
	@Override
	public void run() {		
		long timeLeft = event.getSecondsBeforeStart();
		
		if(event.getState().equals(EventState.TEASING)){
			if(displayAt.contains(event.getSecondsBeforeStart())){
				event.broadcastMessage( ChatColor.GREEN+"L'event "+
										ChatColor.WHITE+ChatColor.BOLD+event.getType()+
										ChatColor.RESET+ChatColor.GREEN+" va commencer dans "+
										ChatColor.WHITE+ChatColor.BOLD+Time.getFormattedTime(timeLeft)+
										ChatColor.RESET+ChatColor.GREEN+" !", Sound.CLICK);
			}
			
			if(timeLeft == 0l){
				event.broadcastMessage( ChatColor.GREEN+"L'event "+
						ChatColor.WHITE+ChatColor.BOLD+event.getType()+
						ChatColor.RESET+ChatColor.GREEN+" commence maintnenant ! ", Sound.ENDERDRAGON_GROWL);
				event.start();
			}else{
				event.getScoreboard().updateAllPlayers();
				timeLeft--;
				event.setSecondsBeforeStart(timeLeft);
				Bukkit.getScheduler().runTaskLater(Survie.getPlugin(), this,20);
			}
		}
	}

}
