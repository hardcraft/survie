package com.gmail.val59000mc.survie.events;


public interface IEventHC {
	public void startTeaserAfterDelay(long delayed);
	public void startTeaser();
	public void start();
	public void end();
	public void remove();
	public EventState getState();
	public EventType getType();
	public EventScoreboard getScoreboard();
	public String getEventSummary();
}
