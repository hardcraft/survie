package com.gmail.val59000mc.survie.exceptions;

public class AdminChatPlayerNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5195201424953263777L;

	public AdminChatPlayerNotFoundException(String message){
		super(message);
	}
}
