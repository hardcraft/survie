package com.gmail.val59000mc.survie.exceptions;

public class EventAlreadyScheduledException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = -2130044877941940943L;

	public EventAlreadyScheduledException(String message){
		super(message);
	}
}
