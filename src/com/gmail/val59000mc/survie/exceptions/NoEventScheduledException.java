package com.gmail.val59000mc.survie.exceptions;

public class NoEventScheduledException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3849318731149447758L;

	public NoEventScheduledException(String message){
		super(message);
	}
}
