package com.gmail.val59000mc.survie.exceptions;

public class PlayerNotOnlineException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1198027512754042790L;

	public PlayerNotOnlineException(String message){
		super(message);
	}
}
