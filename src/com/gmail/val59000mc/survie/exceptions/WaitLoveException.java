package com.gmail.val59000mc.survie.exceptions;

public class WaitLoveException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = -2716789996199186266L;

	public WaitLoveException(String message){
		super(message);
	}
}
