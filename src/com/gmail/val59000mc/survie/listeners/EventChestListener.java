package com.gmail.val59000mc.survie.listeners;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.gmail.val59000mc.survie.events.EventChest;

public class EventChestListener implements Listener{
	
	private Set<Location> locations;
	private EventChest eventChest;
	
	public EventChestListener(EventChest eventChest, Set<Location> locations){
		this.locations = locations;
		this.eventChest = eventChest;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerChestAccess(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType().equals(Material.CHEST)){
			Location chestLocation = event.getClickedBlock().getLocation();
			if(locations.contains(chestLocation)){
				eventChest.chestFound(player,chestLocation);
			}
	    }
	}
}
