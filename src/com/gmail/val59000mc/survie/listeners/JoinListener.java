package com.gmail.val59000mc.survie.listeners;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.survie.Survie;
import com.gmail.val59000mc.survie.dependencies.PermissionsExManager;

import ca.wacos.nametagedit.NametagAPI;

public class JoinListener implements Listener{

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){
		
		UUID uuid = event.getPlayer().getUniqueId();
		
		Bukkit.getScheduler().runTaskLater(Survie.getPlugin(), new Runnable() {
			
			@Override
			public void run() {

				Player player = Bukkit.getPlayer(uuid);
				if(player == null)
					return;
				
				String prefix = PermissionsExManager.getShortPrefix(player.getName());
				Logger.debug(prefix+"&f");
				NametagAPI.setPrefix(player.getName(), prefix+"&f");
				
			}
		}, 20);
		
	}
	
}
