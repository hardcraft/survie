package com.gmail.val59000mc.survie.listeners;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.gmail.val59000mc.survie.VaultManager;
import com.gmail.val59000mc.survie.managers.HCTeamManager;
import com.gmail.val59000mc.survie.players.HCPlayer;
import com.gmail.val59000mc.survie.players.HCTeam;

public class PlayerChatListener implements Listener{
	
	private VaultManager vault = VaultManager.getInstance();
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerChat(AsyncPlayerChatEvent event){
		
		if(event.isCancelled()){
			return;
		}
		
		//Remplacer bvn par Bienvenue
		if(event.getMessage().toLowerCase().contains("bvn")){
			event.setMessage(event.getMessage().toLowerCase().replace("bvn", "Bienvenue !"));
		}
		
		Player player = event.getPlayer();
		HCTeamManager teamManager = HCTeamManager.getInstance();
		
		if(teamManager.hasTeam(player)){
			HCTeam team = teamManager.getTeam(player);
			HCPlayer hcPlayer = team.getPlayer(player);
			
			if(hcPlayer.isTalkingInTeamChat()){
				
				String prefix = ChatColor.WHITE+"["+
						  ChatColor.GOLD+team.getName()+
						  ChatColor.WHITE+"] "+
						  ChatColor.GOLD+"Team "+
						  ChatColor.WHITE+": "+
						  ChatColor.RESET;
				
				
				event.getRecipients().clear();
				event.setMessage(prefix+ChatColor.translateAlternateColorCodes('&', vault.getSuffix(player))+event.getMessage());

				addRecipient(event.getRecipients(),team.getLeader());
				for(HCPlayer teammate : team.getMembers()){
					addRecipient(event.getRecipients(),teammate);
				}
			}else{
				
				String prefix = ChatColor.WHITE+"["+
						  ChatColor.GRAY+team.getName()+
						  ChatColor.WHITE+"] "+ChatColor.RESET;
				event.setMessage(prefix+ChatColor.translateAlternateColorCodes('&', vault.getSuffix(player))+event.getMessage());
			}
		}
		
		
		
	}

	private void addRecipient(Set<Player> recipients, HCPlayer teammate) {

		Player recipient = Bukkit.getPlayer(teammate.getName());
		if(recipient != null){
			recipients.add(recipient);
		}
		
	}
}
