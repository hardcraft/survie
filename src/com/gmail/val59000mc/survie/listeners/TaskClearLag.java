package com.gmail.val59000mc.survie.listeners;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.survie.Survie;

public class TaskClearLag implements Runnable{
	
	Runnable task;
	
	public TaskClearLag() {
		task = this;
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(Survie.getPlugin(), new Runnable(){

			@Override
			public void run() {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lagg unloadchunks");
				Bukkit.getLogger().info("Running scheduled (15 min) /clearlagg unloadchunk");
				Bukkit.getScheduler().runTaskLaterAsynchronously(Survie.getPlugin(), task, 18000);
				
			}
			
		});
	}
}
