package com.gmail.val59000mc.survie.managers;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.survie.events.EventState;
import com.gmail.val59000mc.survie.events.IEventHC;
import com.gmail.val59000mc.survie.exceptions.EventAlreadyScheduledException;
import com.gmail.val59000mc.survie.exceptions.NoEventScheduledException;

public class EventManager {
	private static EventManager instance;
	
	private IEventHC event;
	
	public EventManager(){
		instance = this;
		this.event = null;
	}
	
	public static EventManager getInstance(){
		return instance;
	}
	
	public IEventHC scheduleEvent(IEventHC event, long delayed) throws EventAlreadyScheduledException{
		if(this.event == null || this.event.getState().equals(EventState.REMOVED)){
			this.event = event;
			event.startTeaserAfterDelay(delayed);
			return event;
		}else{
			throw new EventAlreadyScheduledException("Un event de type "+this.event.getType().toString()+" est déjà programmé ou en cours");
		}
		
	}

	public void stopEvent() throws NoEventScheduledException {
		if(this.event == null || this.event.getState().equals(EventState.REMOVED)){
			throw new NoEventScheduledException("Aucun event en cours");
		}else{
			this.event.remove();
		}
	}

	public Boolean toggleScoreboardVisibility(Player player) throws NoEventScheduledException {
		if(this.event == null || this.event.getState().equals(EventState.REMOVED)){
			throw new NoEventScheduledException("Aucun event en cours");
		}else{
			return this.event.getScoreboard().toggleVisibility(player);
		}
	}
	
	public String getCurrentEventInfo() throws NoEventScheduledException{
		if(this.event == null || this.event.getState().equals(EventState.REMOVED)){
			throw new NoEventScheduledException("Aucun event en cours");
		}else{
			return event.getEventSummary();
		}
	}
}
