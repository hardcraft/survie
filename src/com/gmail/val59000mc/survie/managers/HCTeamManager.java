package com.gmail.val59000mc.survie.managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.survie.Survie;
import com.gmail.val59000mc.survie.players.HCPlayer;
import com.gmail.val59000mc.survie.players.HCTeam;

public class HCTeamManager {

	private static HCTeamManager instance;
	
	private List<HCTeam> teams;
	
	public HCTeamManager(){
		HCTeamManager.instance = this;
		this.teams = Collections.synchronizedList(new ArrayList<HCTeam>());;
		loadAllTeams();
	}
	
	public static HCTeamManager getInstance(){
		return instance;
	}
	
	public void loadAllTeams(){
		File file = new File(Survie.getPlugin().getDataFolder(),"VIPteams.yml");
		try {
			if(!file.exists()){
				file.createNewFile();
			}
			
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			ConfigurationSection teamSection = cfg.getConfigurationSection("teams");
			for(String key : teamSection.getKeys(false)){
				
				if(!key.equals("start")){
					HCTeam team = new HCTeam();

					// set name
					team.setName(teamSection.getString(key+".name"));
					
					// set leader
					String[] leaderString = teamSection.getString(key+".leader").split(" ");
					HCPlayer leader = new HCPlayer(leaderString[0],UUID.fromString(leaderString[1]));
					team.setLeader(leader);
					
					// set members
					for(String memberKey : teamSection.getStringList(key+".members")){
						String[] memberString = memberKey.split(" ");
						HCPlayer member = new HCPlayer(memberString[0],UUID.fromString(memberString[1]));
						team.addMember(member);
					}
					
					// set home
					String worldString = teamSection.getString(key+".home");
					if(worldString != "no"){
						String[] worldArray = teamSection.getString(key+".home").split(" ");
						World world = Bukkit.getWorld(worldArray[0]);
						if(world != null){
							team.setHome(new Location(world,Double.parseDouble(worldArray[1]),Double.parseDouble(worldArray[2]),Double.parseDouble(worldArray[3])));
						}
						
					}
					
					teams.add(team);
				}
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HCPlayer switchTeamChat(Player player) {
		HCTeam team = getTeam(player);
		HCPlayer hcPlayer = team.getPlayer(player);
		hcPlayer.setTalkingInTeamChat(!hcPlayer.isTalkingInTeamChat());
		return hcPlayer;
	}


	public void createTeam(Player player, String name) throws IOException {
		HCTeam team = new HCTeam();
		team.setLeader(new HCPlayer(player.getName(), player.getUniqueId()));
		team.setName(name);
		team.save();
		teams.add(team);
	}


	public void invite(HCTeam team, Player invited) {
		team.invite(invited);
	}
	
	
	public HCTeam getTeam(Player player){
		synchronized(teams){
			for(HCTeam team : teams){
				if(team.isInTeam(player)){
					return team;
				}
			}
			return null;
		}
	}
	
	public boolean hasTeam(Player player){
		synchronized(teams){
			for(HCTeam team : teams){
				if(team.isInTeam(player)){
					return true;
				}
			}
			return false;
		}
	}
	
	public boolean teamNameExists(String name){
		synchronized(teams){
			for(HCTeam team : teams){
				if(team.getName().equals(name)){
					return true;
				}
			}
			return false;
		}
	}
	
	public void removeTeam(HCTeam team) throws IOException{
		synchronized(teams){
			File file = new File(Survie.getPlugin().getDataFolder(),"VIPteams.yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			cfg.set("teams."+team.getName(), null);
			try {
				cfg.save(file);
				teams.remove(team);
			} catch (IOException e) {
				teams.add(team);
				e.printStackTrace();
				throw e;
			}
		}
		
	}

	public HCTeam getTeam(String name) {
		synchronized(teams){
			for(HCTeam team : teams){
				if(team.getName().equals(name)){
					return team;
				}
			}
			return null;
		}
		
	}
	
}
