package com.gmail.val59000mc.survie.managers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.survie.Survie;
import com.gmail.val59000mc.survie.exceptions.WaitLoveException;

public class LoveBoorinooManager {
	private Map<String,Long> players;
	private static LoveBoorinooManager instance;
	
	public LoveBoorinooManager(){
		players = Collections.synchronizedMap(new HashMap<String,Long>());
		LoveBoorinooManager.instance = this;
	}
	
	public static LoveBoorinooManager getInstance(){
		return instance;
	}
	
	public void loveBoorinoo(Player player) throws WaitLoveException{
		Long time = players.get(player.getName());
		if(time == null || player.isOp()){
			doLoveBoorinoo(player);
			return;
		}
		
		Long now = Calendar.getInstance().getTimeInMillis();
		if(now-time > 21600000){
			doLoveBoorinoo(player);
		}else{
			long left = (21600000-(now-time))/1000;
			throw new WaitLoveException(ChatColor.RED+"Vous devez attendre encore "+Time.getFormattedTime(left)+" avant d'aimer Boorinoo à nouveau");
		}
	}
	
	private void doLoveBoorinoo(final Player player){
		
		Long now = Calendar.getInstance().getTimeInMillis();
		players.put(player.getName(), now);
		
		final ItemStack cookie = new ItemStack(Material.COOKIE,1);
		ItemMeta meta = cookie.getItemMeta();
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GREEN+"Cadeau de Boorinoo");
		meta.setLore(lore);
		cookie.setItemMeta(meta);
		
		final Random r = new Random();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Survie.getPlugin(), new Runnable() {

			int count = 0;
			
			
			@Override
			public void run() {
				try {
					Location loc = player.getLocation().clone().add(0,2,0);
					for(Player p : Bukkit.getOnlinePlayers()){
						p.spigot().playEffect(loc, Effect.HEART, 0, 0, 0.6f, 0.6f, 0.6f, 1, 15, 50);
						p.playSound(loc, Sound.LEVEL_UP, 0.5f, 2f);
					}
					spawnRandomFirework(loc);
					spawnRandomCookie(loc);
				} catch (Exception e) {
					Bukkit.getLogger().severe(e.getMessage());
				}
				count++;
				if(count < 20){
					Bukkit.getScheduler().scheduleSyncDelayedTask(Survie.getPlugin(), this, 5);
				}
			}
			
			private void spawnRandomFirework(Location location){
				//Spawn the Firework, get the FireworkMeta.
		        Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
		        FireworkMeta fwm = fw.getFireworkMeta();
		       
		        //Our random generator
		        Random r = new Random();  

		        //Get the type
		        int rt = r.nextInt(4) + 1;
		        Type type = Type.BALL;      
		        if (rt == 1) type = Type.BALL;
		        if (rt == 2) type = Type.BALL_LARGE;
		        if (rt == 3) type = Type.BURST;
		        if (rt == 4) type = Type.CREEPER;
		        if (rt == 5) type = Type.STAR;
		       
		        //Get our random colours  
		        int red = r.nextInt(256);
		        int green = r.nextInt(256);
		        int blue = r.nextInt(256);
		        Color c1 = Color.fromRGB(red, green, blue);

		        red = r.nextInt(256);
		        green = r.nextInt(256);
		        blue = r.nextInt(256);
		        Color c2 = Color.fromRGB(red, green, blue);
		       
		        //Create our effect with this
		        FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
		      
		        //Then apply the effect to the meta
		        fwm.addEffect(effect);
		       
		        //Generate some random power and set it
		        int rp = r.nextInt(2) + 1;
		        fwm.setPower(rp);
		       
		        //Then apply this to our rocket
		        fw.setFireworkMeta(fwm);          
			}
			
			private void spawnRandomCookie(Location location){
				ChatColor randomColor = ChatColor.values()[r.nextInt(ChatColor.values().length)];
				ItemMeta meta = cookie.getItemMeta();
				meta.setDisplayName(ChatColor.RED+"♥ ♥ ♥ "+randomColor+"I Love Boorinoo"+ChatColor.RED+" ♥ ♥ ♥");
				ItemStack newCookie = cookie.clone();
				newCookie.setItemMeta(meta);
				
				Item item = player.getWorld().dropItemNaturally(location, newCookie);
				item.setVelocity(new Vector(2.0*r.nextDouble()-1,2.0*r.nextDouble()-1,2.0*r.nextDouble()-1));
			}
			
		}, 5);
		
	}
	
	
}
