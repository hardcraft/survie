package com.gmail.val59000mc.survie.players;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class HCPlayer {
	private String name;
	private UUID uuid;
	private boolean talkingInTeamChat;
	
	public HCPlayer(String name, UUID uuid){
		this.name = name;
		this.uuid = uuid;
		this.talkingInTeamChat = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	public boolean nameEquals(String name){
		return this.name.equals(name);
	}
	
	public boolean uuidEquals(UUID uuid){
		return this.uuid.equals(uuid);
	}

	public boolean isTalkingInTeamChat() {
		return talkingInTeamChat;
	}

	public void setTalkingInTeamChat(boolean talkingInTeamChat) {
		this.talkingInTeamChat = talkingInTeamChat;
	}
	
	public String getConfigString(){
		
		// Fix changing name everytime we try to get the config string
		Player p = Bukkit.getPlayer(getUuid());
		if(p != null){
			this.name = p.getName();
		}
		
		return name+" "+uuid.toString();
	}
}
