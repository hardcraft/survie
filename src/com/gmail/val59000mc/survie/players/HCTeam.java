package com.gmail.val59000mc.survie.players;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.survie.Survie;

public class HCTeam {
	private HCPlayer leader;
	private List<HCPlayer> members;
	private Location home;
	private Map<HCPlayer,Long> pendingRequests;
	private String name;
	
	
	public HCTeam(){
		members = Collections.synchronizedList(new ArrayList<HCPlayer>());
		pendingRequests = new HashMap<HCPlayer,Long>();
		home = null;
		name = "";
	}



	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public HCPlayer getLeader() {
		return leader;
	}

	public void setLeader(HCPlayer leader) {
		this.leader = leader;
	}

	public List<HCPlayer> getMembers() {
		return members;
	}

	public void setMembers(List<HCPlayer> members) {
		this.members = members;
	}

	public Location getHome() {
		return home;
	}

	public void setHome(Location home) {
		this.home = home;
	}

	public Map<HCPlayer,Long> getPendingRequests() {
		return pendingRequests;
	}

	public void setPendingRequests(Map<HCPlayer,Long> pendingRequests) {
		this.pendingRequests = pendingRequests;
	}
	
	public void invite(Player player){
		Long now = Calendar.getInstance().getTimeInMillis();
		
		getPendingRequests().put(new HCPlayer(player.getName(), player.getUniqueId()),now);
		
		player.sendMessage(ChatColor.DARK_AQUA+leader.getName()+
						   ChatColor.AQUA+" t'invite dans sa team, tape "+
						   ChatColor.WHITE+"/team accept "+leader.getName()+
						   ChatColor.AQUA+" pour accepter (commande valable pendant 60 secondes).");
	}
	
	public boolean isInvited(Player player){
		Long now = Calendar.getInstance().getTimeInMillis();
		
		for(Entry<HCPlayer,Long> entry : getPendingRequests().entrySet()){
			Long delay = now - entry.getValue();
			if(delay < 60000 && entry.getKey().getUuid().equals(player.getUniqueId())){
				return true;
			}
		}
		
		return false;
	}
	
	public void join(Player player) throws IOException{
		
		// Remove pending request
		HCPlayer newPlayer = null;
		for(Entry<HCPlayer,Long> entry : getPendingRequests().entrySet()){
			HCPlayer hcPlayer = entry.getKey();
			if(hcPlayer.getUuid().equals(player.getUniqueId())){
				newPlayer = hcPlayer;
			}
		}
		getPendingRequests().remove(newPlayer);
		
		
		addMember(newPlayer);
		try {
			save();
		} catch (IOException e) {
			getMembers().remove(newPlayer);
			throw e;
		}
		
	}
	
	public void modifyHome(Location location) throws IOException{
		Location old = getHome();
		setHome(location);
		try {
			save();
		} catch (IOException e) {
			setHome(old);
			e.printStackTrace();
			throw e;
		}
	}
	
	public void leave(Player player) throws IOException{
		HCPlayer removed = getPlayer(player);
		getMembers().remove(getPlayer(player));
		try {
			save();
		} catch (IOException e) {
			addMember(removed);
			e.printStackTrace();
			throw e;
		}
	}
	
	public void summonPlayers(Player toPlayer){
		if(!isLeader(toPlayer)){
			Player p = Bukkit.getPlayer(getLeader().getName());
			if(p != null){
				Bukkit.getServer().dispatchCommand(toPlayer, "tpahere "+p.getName());
				p.sendMessage(ChatColor.GREEN+toPlayer.getName()+" demande un rassemblement de team !");
				p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1.2f);
			}
		}
		synchronized (getMembers()) {
			for(HCPlayer hcPlayer : getMembers()){
	
				if(hcPlayer.getUuid() != toPlayer.getUniqueId()){
					Player p = Bukkit.getPlayer(hcPlayer.getName());
					if(p != null){
						Bukkit.getServer().dispatchCommand(toPlayer, "tpahere "+p.getName());
						p.sendMessage(ChatColor.GREEN+toPlayer.getName()+" demande un rassemblement de team !");
						p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1.2f);
					}
				}
			}
		}
	}

	public void addMember(HCPlayer player){
		getMembers().add(player);
	}
	
	public boolean isInTeam(Player player){
		UUID playerUUID = player.getUniqueId();
		
		if(leader.uuidEquals(playerUUID)){
			return true;
		}
		synchronized (getMembers()) {
			for(HCPlayer p : getMembers()){
				if(p.uuidEquals(playerUUID)){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean isLeader(Player player){
		return getLeader().getUuid().equals(player.getUniqueId());
	}
	
	public HCPlayer getPlayer(Player player){

		UUID playerUUID = player.getUniqueId();
		
		if(leader.uuidEquals(playerUUID)){
			return leader;
		}
		synchronized (getMembers()) {
			for(HCPlayer p : getMembers()){
				if(p.uuidEquals(playerUUID)){
					return p;
				}
			}
		}
		
		return null;
	}
	
	public void save() throws IOException{
		File file = new File(Survie.getPlugin().getDataFolder(),"VIPteams.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		// name
		cfg.set("teams."+getName()+".name", getName());
		
		// leader
		cfg.set("teams."+getName()+".leader", getLeader().getConfigString());
		
		// members
		List<String> memberList = new ArrayList<String>();
		synchronized (getMembers()) {
			for(HCPlayer member : getMembers()){
				memberList.add(member.getConfigString());
			}
		}
		cfg.set("teams."+getName()+".members", memberList);
		
		// location
		String locationString;
		if(getHome() == null){
			locationString = "no";
		}else{
			locationString = getHome().getWorld().getName()+" "+getHome().getBlockX()+" "+getHome().getBlockY()+" "+getHome().getBlockZ();
		}
		cfg.set("teams."+getName()+".home", locationString);
		
		cfg.save(file);
	}
	
	public void broadcastMessage(String message, Player exceptionPlayer){
		
		if(exceptionPlayer == null || (exceptionPlayer != null && !getLeader().getUuid().equals(exceptionPlayer.getUniqueId())) ){
			Player p = Bukkit.getPlayer(getLeader().getUuid());
			if(p != null){
				p.sendMessage(message);
			}
		}
		
		synchronized(getMembers()){
			for(HCPlayer hcPlayer : getMembers()){
				Player p = Bukkit.getPlayer(hcPlayer.getUuid());
				if(p != null && (exceptionPlayer == null || (exceptionPlayer != null && !p.getUniqueId().equals(hcPlayer.getUuid())))){
					p.sendMessage(message);;
				}
			}
		}
	}



	public void rename(String name) throws IOException {
		String old = getName();
		setName(name);
		try {
			File file = new File(Survie.getPlugin().getDataFolder(),"VIPteams.yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			cfg.set("teams."+old, null);
			cfg.save(file);
			save();
		} catch (IOException e) {
			setName(old);
			e.printStackTrace();
			throw e;
		}
		
	}
}
